import { AxiosResponse } from 'axios'

import ApiService from '@/services/_helper/api.service'

export default class TourService {
  static getTour (): Promise<AxiosResponse> {
    return ApiService.get('/tours?populate=*')
  }
}
