import { AxiosResponse } from 'axios'

import ApiService from '@/services/_helper/api.service'

export default class AboutService {
  static getAbout (): Promise<AxiosResponse> {
    return ApiService.get('/abouts?populate=*')
  }
}
