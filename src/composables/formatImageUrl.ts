import { ImageInterface } from '@/types/image.interface'

const formatImageUrl = (product: ImageInterface | undefined): string => {
  return 'http://localhost:1337' + product?.attributes.url + '?updated_at=' + product?.attributes.updatedAt
}
export default formatImageUrl
