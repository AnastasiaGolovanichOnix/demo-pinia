import { TourInterface } from '@/types/tour.interface'

const getTourDuration = (tour: TourInterface | undefined) => {
  const start = tour?.attributes.startDate
  const end = tour?.attributes.endDate
  if (start && end) return (new Date(end).getTime() - new Date(start).getTime()) / 86400000
}
export default getTourDuration
