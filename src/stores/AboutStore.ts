import { defineStore } from 'pinia'

import { AboutInterface } from '@/types/about.interface'
import { AboutStoreInterface } from '@/types/store/aboutStore.interface'

export const useAboutStore = defineStore({
  id: 'aboutStore',
  state: () => ({
    about: {} as AboutInterface
  } as AboutStoreInterface),
  getters: {
    getAbout: (state) => {
      return state.about
    }
  },
  actions: {
    setAbout (res: AboutInterface) {
      this.about = res
    }
  }
})
