import { defineStore } from 'pinia'

import { TourInterface } from '@/types/tour.interface'
import { TourStoreInterface } from '@/types/store/tourStore.interface'
import { TestimonialInterface } from '@/types/testimonial.interface'
import { ImageInterface } from '@/types/image.interface'

export const useTourStore = defineStore({
  id: 'tourStore',
  state: () => ({
    tours: [] as TourInterface[]
  } as TourStoreInterface),
  getters: {
    getTour: (state) => {
      return state.tours
    },
    getMaxIndex: (state) => {
      return Math.max(...state.tours.map(item => item.id))
    },
    getTourCount: (state) => {
      return state.tours.length
    },
    getTourById: (state) => {
      return (userId: number) => state.tours.find((user) => user.id === userId)
    },
    getTestimonials: (state) => {
      const testimonials = [] as TestimonialInterface[]
      state.tours.forEach((item) => testimonials.push(...item.attributes.testimonial))
      return testimonials
    },
    getTestimonialById: (state) => {
      const store = useTourStore()
      return (testimonialId: number) => store.getTestimonials.find((testimonial) => testimonial.id === testimonialId)
    },
    getImages: (state) => {
      const images = [] as ImageInterface[]
      state.tours.forEach((item) => images.push(...item.attributes.images.data))
      return images
    }
  },
  actions: {
    setTour (res: TourInterface[]) {
      this.tours = res
    }
  }
})
