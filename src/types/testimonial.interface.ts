export interface TestimonialInterface {
    id: number
    name: string
    testimonial: string
    rating: number
}
