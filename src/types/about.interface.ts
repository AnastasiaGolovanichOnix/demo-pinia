import { ImageInterface } from '@/types/image.interface'

export interface AboutInterface {
  id: number,
  attributes: {
    aboutUs: string
    images: {data: ImageInterface[]}
    publishedAt: string
    createdAt: string
    updatedAt: string
  }
}
