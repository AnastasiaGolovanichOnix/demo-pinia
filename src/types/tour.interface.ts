import { ImageInterface } from '@/types/image.interface'
import { TestimonialInterface } from '@/types/testimonial.interface'

export interface TourInterface {
  id: number,
  attributes: {
    createdAt: string
    description: string
    endDate: string
    images: {data: ImageInterface[]}
    longDescription: string
    mainImage: {data: ImageInterface}
    name: string
    price: number
    publishedAt: string
    startDate: string
    updatedAt: string
    testimonial: TestimonialInterface[]
  }
}
