import { TourInterface } from '@/types/tour.interface'

export interface TourStoreInterface {
  tours: TourInterface[]
}
