import { AboutInterface } from '@/types/about.interface'

export interface AboutStoreInterface {
  about: AboutInterface
}
