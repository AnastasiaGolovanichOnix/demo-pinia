export enum RouteName {
  Home = 'Home',
  AboutUs = 'AboutUs',
  Gallery = 'Gallery',
  Testimonials = 'Testimonials',
  Tours = 'Tours',
  Tour = 'Tour',
}
