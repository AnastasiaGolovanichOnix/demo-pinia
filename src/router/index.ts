import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import { RouteName } from '@/enums/RouteName'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    component: () => import(/* webpackChunkName: "layout" */ '../layouts/TheLayout.vue'),
    children: [
      {
        path: '/',
        name: RouteName.Home,
        component: () => import(/* webpackChunkName: "layout" */ '../views/HomePage.vue')
      },
      {
        path: 'about-us',
        name: RouteName.AboutUs,
        component: () => import(/* webpackChunkName: "about-us" */ '../views/AboutUs.vue')
      },
      {
        path: 'tours',
        name: RouteName.Tours,
        component: () => import(/* webpackChunkName: "tours" */ '../views/ToursPage.vue')
      },
      {
        path: 'tour/:id',
        name: RouteName.Tour,
        component: () => import(/* webpackChunkName: "tours" */ '../views/TourInfo.vue')
      },
      {
        path: 'testimonials',
        name: RouteName.Testimonials,
        component: () => import(/* webpackChunkName: "testimonials" */ '../views/TestimonialsPage.vue')
      },
      {
        path: 'gallery',
        name: RouteName.Gallery,
        component: () => import(/* webpackChunkName: "gallery" */ '../views/GalleryPage.vue')
      }
    ]
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
