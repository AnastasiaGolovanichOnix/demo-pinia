module.exports = {
  css: {
    loaderOptions: {
      sass: {
        additionalData: `
        @import '~@/scss/core/_variables.scss';
        @import '~@/scss/core/_media.scss';
        @import '~@/scss/core/_colors.scss';
        `
      }
    }
  }
}
