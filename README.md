# demo-pinia

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Use Strapi for Api:
```
email - anastasiia.holovanych@onix-systems.com
password - Qwerty12345

```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
